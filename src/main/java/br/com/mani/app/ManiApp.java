package br.com.mani.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.ComponentScan;

import br.com.mani.app.swt.component.ManiSplash;
import br.com.mani.app.swt.component.UIManager;

@ComponentScan("br.com.mani.app")
public class ManiApp implements ApplicationListener<ApplicationReadyEvent>{

	@Autowired
	private UIManager uiManager;
	
	public static void main(String[] args) {
		
		final ManiSplash splash = new ManiSplash();

		splash.setStartupTask(() -> SpringApplication.run(ManiApp.class, args));
		
		splash.start();
		
	}

	@Override
	public void onApplicationEvent(ApplicationReadyEvent event) {
		
		this.uiManager.startApp();
	
	}
	
}
