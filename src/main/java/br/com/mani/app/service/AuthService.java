package br.com.mani.app.service;

import org.springframework.stereotype.Service;

import br.com.mani.app.mvc.model.LoginModel;

@Service
public class AuthService {

	public Long autenticar(LoginModel login) {
		
		if(login.getUsuario().equals("bruno") && login.getSenha().equals("123")) {
			return 1L;
		}
		
		throw new SecurityException("login inválido");
	}
	
}
