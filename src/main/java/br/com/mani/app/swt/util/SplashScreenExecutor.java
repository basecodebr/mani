package br.com.mani.app.swt.util;


import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.function.BooleanSupplier;

import org.apache.commons.lang.StringUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;

import br.com.mani.app.swt.core.InitializationTask;
import br.com.mani.app.swt.core.UIInitializationTask;

public class SplashScreenExecutor <T> {

	private String image;
	
	private T applicationContainer;
	
	private Shell applicationShell;
	
	private final Display display = Display.getDefault();
	
	private boolean centralize;
	
	private Runnable onComplete;
	
	private BooleanSupplier onCompleteCondition;

	private List<InitializationTask<T>> tasks = new ArrayList<>();
	
	private UIInitializationTask<T> uiTask;

	public SplashScreenExecutor<T> uiTask(UIInitializationTask<T> uiTask){
		this.uiTask = uiTask;
		return this;
	}

	public SplashScreenExecutor<T> onComplete(Runnable runnable, BooleanSupplier condition){
		this.onComplete = runnable;
		this.onCompleteCondition = condition;
		return this;
	}

	public SplashScreenExecutor<T> task(InitializationTask<T> task){
		this.tasks.add(task);
		return this;
	}

	public SplashScreenExecutor(T runner) {
		this.applicationContainer = runner;
	}

	public SplashScreenExecutor<T> centralize(boolean centralize){
		this.centralize = centralize;
		return this;
	}

	public SplashScreenExecutor<T> img(String image){
		this.image = image;
		return this;
	}

	private Image _image;
	
	public void splash() {

		Locale.setDefault(new Locale("pt","BR"));
		
		final Shell splash = new Shell(SWT.ON_TOP);
		splash.setBackground(Display.getDefault().getSystemColor(SWT.COLOR_WHITE));
		splash.setBackgroundMode(SWT.INHERIT_FORCE);
		splash.setLayout(new GridLayout());

		if(StringUtils.isNotBlank(this.image)){
			Label label = new Label(splash, SWT.NONE);
			this._image = ResourceUtil.getImage(this.image, ResourceConstants.ImageHight);
			label.setImage(this._image);
			label.setLayoutData(new GridData(GridData.FILL, SWT.FILL,true,true,1,1));
			label.setAlignment(SWT.CENTER);
		}

		final ProgressBar bar = new ProgressBar(splash, SWT.NONE);
		bar.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		Label textLabel = new Label(splash, SWT.NONE);
		textLabel.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		bar.setMaximum(this.tasks.size());
		splash.pack();
		Rectangle splashRectangle = splash.getBounds();
		Rectangle displayRectangle = Display.getCurrent().getClientArea();

		int xLocation = (displayRectangle.width - splashRectangle.width) / 2;
		int yLocation = (displayRectangle.height - splashRectangle.height) / 2;

		splash.setLocation(xLocation, yLocation);

		splash.open();

		this.display.asyncExec(new Runnable() {
			int taskCount = 0;
			@Override
			public void run() {

				try{
					
					T session = SplashScreenExecutor.this.applicationContainer;
					
					for(InitializationTask<T> task:SplashScreenExecutor.this.tasks){
					
						textLabel.setText(task.getDescription(session));
						
						task.execute(session);
						
						bar.setSelection(++this.taskCount);
					}

					SplashScreenExecutor.this.applicationShell = SplashScreenExecutor.this.uiTask.execute(session);

					openApplicationShell();
					
					splash.close();

					if(SplashScreenExecutor.this._image != null){
					
						SplashScreenExecutor.this._image.dispose();
					
					}

				}catch(Exception e){
					
					e.printStackTrace();
					
					splash.close();
				}

				if (SplashScreenExecutor.this.onComplete != null && (SplashScreenExecutor.this.onCompleteCondition == null || SplashScreenExecutor.this.onCompleteCondition.getAsBoolean())) {
					
					SplashScreenExecutor.this.onComplete.run();

				}


			}
		});


		while (this.applicationShell == null || !this.applicationShell.isDisposed()) {
			if (!this.display.readAndDispatch ()) this.display.sleep ();
		}
		this.display.dispose();
		System.exit(0);
	}


	private void openApplicationShell(){
		Monitor primary = this.display.getPrimaryMonitor();
		Rectangle bounds = primary.getClientArea();

		if(this.centralize){
			Rectangle rect = this.applicationShell.getClientArea();
			int x = bounds.x + (bounds.width - rect.width) / 2;
			int y = bounds.y + (bounds.height - rect.height) / 2;
			this.applicationShell.setLocation(x,y);
		}else{
			this.applicationShell.setBounds(bounds);
			this.applicationShell.setLocation(0,0);
			this.applicationShell.setMaximized(true);
		}

		this.applicationShell.setAlpha(255);
		this.applicationShell.setActive();
		this.applicationShell.open();

	}

}