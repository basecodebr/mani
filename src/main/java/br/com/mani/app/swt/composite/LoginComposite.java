package br.com.mani.app.swt.composite;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import br.com.mani.app.mvc.controller.LoginController;
import br.com.mani.app.mvc.model.LoginModel;
import br.com.mani.app.swt.util.ResourceConstants;
import br.com.mani.app.swt.util.ResourceUtil;

public class LoginComposite extends Composite{

	private Text txtUser;
	
	private Text txtPass;
	
	private Button ckCreateDefaults;
	
	public static final String ID = "loginComposite";
	
	private LoginController controller;

	public LoginComposite(Composite parent, int style, LoginController controller) {
		super(parent, style);
		createContents();
		this.controller = controller;
	}

	private void createContents(){

		Label label = new Label(this, SWT.NONE);

		label.setImage(ResourceUtil.getImage("logo-login2.png", ResourceConstants.ImageHight));
		FormData formData = new FormData();
		formData.top = new FormAttachment(20);
		formData.left = new FormAttachment(0);
		formData.right = new FormAttachment(50);
		label.setLayoutData(formData);

		setLayout(new FormLayout());
		setBackground(Display.getDefault().getSystemColor(SWT.COLOR_WHITE));
		setBackgroundMode(SWT.INHERIT_FORCE);

		formData = new FormData();
		formData.top = new FormAttachment(0);
		formData.left = new FormAttachment(label, 10);
		formData.bottom = new FormAttachment(100);

		label = new Label(this, SWT.SEPARATOR | SWT.VERTICAL);
		label.setLayoutData(formData);

		Composite composite = new Composite(this, SWT.NONE);
		composite.setLayout(new GridLayout(2,true));
		formData = new FormData();
		formData.top = new FormAttachment(45);
		formData.left = new FormAttachment(label);
		composite.setLayoutData(formData);
		composite.setBackgroundMode(SWT.INHERIT_FORCE);


		//		if(this.model.firstAccess){
		//			label = new Label(composite, SWT.NONE);
		//			label.setText(message(Statizo.STR_SECURITY_ORIENTATION_FIRST_ACCESS));
		//			label.setLayoutData(new GridData(SWT.FILL, SWT.FILL,true,true,2,1));
		//
		//			label = new Label(composite, SWT.NONE);
		//			label.setText(message(Statizo.STR_SECURITY_ORIENTATION_CHANGE_PASSWORD));
		//			label.setLayoutData(new GridData(SWT.FILL, SWT.FILL,true,true,2,1));
		//
		//			label = new Label(composite, SWT.SEPARATOR | SWT.HORIZONTAL);
		//			label.setLayoutData(new GridData(SWT.FILL,SWT.NONE,true,true,2,1));
		//		}

		label = new Label(composite, SWT.NONE);
		label.setAlignment(SWT.RIGHT);
		label.setText("Usuário");

		GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
		label.setLayoutData(gridData);

		this.txtUser = new Text(composite, SWT.BORDER);
		gridData = new GridData(SWT.FILL, SWT.FILL, true,false);
		gridData.widthHint = 75;
		this.txtUser.setLayoutData(gridData);
		//txtUser.addVerifyListener(new NotSpaceVerifyListener());

		//txtUser.addModifyListener(listener ->  this.model.setUsername(this.txtUser.getText()));

		//		if(this.model.firstAccess){
		//			this.ckCreateDefaults = new Button(composite, SWT.CHECK);
		//			this.ckCreateDefaults.setText(message(Statizo.STR_SECURITY_CREATE_DEFAULTS));
		//			this.ckCreateDefaults.setLayoutData(new GridData(SWT.RIGHT, SWT.FILL,true,true,2,1));
		//			this.ckCreateDefaults.setSelection(true);
		//			this.ckCreateDefaults.addSelectionListener(new SelectionAdapter() {
		//				public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
		//					LoginComposite.this.model.createDefaults = LoginComposite.this.ckCreateDefaults.getSelection();
		//				};
		//			});
		//		}
		//
		label = new Label(composite, SWT.NONE);
		label.setAlignment(SWT.RIGHT);
		label.setText("SENHA");

		gridData = new GridData(SWT.FILL, SWT.CENTER,true,false);
		label.setLayoutData(gridData);

		this.txtPass = new Text(composite, SWT.BORDER | SWT.PASSWORD);
		gridData = new GridData(SWT.FILL, SWT.FILL, true,true);
		gridData.widthHint = 75;
		this.txtPass.setLayoutData(gridData);
		//txtPass.addModifyListener(listener ->  this.model.setPassword(this.txtPass.getText()));


		Button button = new Button(composite, SWT.PUSH);
		
		button.setText("ENTRAR");
		
		gridData = new GridData(SWT.END, SWT.CENTER, true,true);
		
		gridData.horizontalSpan = 2;
		
		button.setLayoutData(gridData);
		
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				autenticar();
			}
		});

	}
	
	private void autenticar() {
		
		this.controller.autenticar(new LoginModel(this.txtUser.getText(), this.txtPass.getText()));
		
	}
	
	

}