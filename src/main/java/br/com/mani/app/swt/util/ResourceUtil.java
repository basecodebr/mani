package br.com.mani.app.swt.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;

import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

public class ResourceUtil {

	private static Map<String, Image> imageCacheMap = new HashMap<>();

	public static Image getImage(String name){
		return getImage(name, ResourceConstants.Image16x16);
	}

	private static Image getFromCache(String name, String type){
		String key = name + "." + type;
		if(imageCacheMap.containsKey(key)){
			return imageCacheMap.get(key);
		}
		return null;
	}

	public static Image getImage(String name, String type){
		if(name == null || name.trim().length() == 0){
			return null;
		}

		try{
			Image image = getFromCache(name, type);
			if(image == null || image.isDisposed()){
				image = new Image(Display.getDefault(),ResourceUtil.class.getClassLoader().getResourceAsStream("img/" + type + "/" +  name));
				imageCacheMap.put(name + "." + type, image);
			}
			return image;
		}catch(Exception e){
			return null;
		}

	}

	public static Properties getPropertiesFile(String name){
		Properties props = new Properties();
		try {
			props.load(ResourceUtil.class.getResourceAsStream("/" + name));
			return props;
		} catch (IOException e) {
			return null;
		}
	}

	public static String getFileText(String fileName){

		try(Scanner in = new Scanner(ResourceUtil.class.getResourceAsStream("/"+fileName))){

			StringBuilder str = new StringBuilder();

			while(in.hasNextLine())
				str.append(in.nextLine()).append("\n");

			return str.toString();

		}

	}
}