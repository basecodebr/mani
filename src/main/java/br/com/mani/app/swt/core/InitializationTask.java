package br.com.mani.app.swt.core;

public interface InitializationTask <T>{

    void execute(T session);
    
    default String getDescription(T session){
    	return "Carregando...";
    }
    
}