package br.com.mani.app.swt.component;

import java.util.Locale;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;

import br.com.mani.app.swt.util.ResourceConstants;
import br.com.mani.app.swt.util.ResourceUtil;

public class ManiSplash{

	private final Display display = Display.getDefault();

	private static final Shell applicationShell = UIManager.applicationShell;

	private static final Shell splashShell = UIManager.splashShell;

	private Runnable startupTask;

	public void start() {

		Locale.setDefault(new Locale("pt","BR"));

		ManiSplash.splashShell.setBackground(Display.getDefault().getSystemColor(SWT.COLOR_WHITE));

		ManiSplash.splashShell.setBackgroundMode(SWT.INHERIT_FORCE);

		ManiSplash.splashShell.setLayout(new GridLayout());

		Label label = new Label(ManiSplash.splashShell, SWT.NONE);

		label.setImage(ResourceUtil.getImage("loading.png", ResourceConstants.ImageHight));

		label.setLayoutData(new GridData(GridData.FILL, SWT.FILL,true,true,1,1));

		label.setAlignment(SWT.CENTER);

		final ProgressBar bar = new ProgressBar(ManiSplash.splashShell, SWT.NONE);

		bar.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		Label textLabel = new Label(ManiSplash.splashShell, SWT.NONE);

		textLabel.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		ManiSplash.splashShell.pack();

		Rectangle splashRectangle = ManiSplash.splashShell.getBounds();

		Rectangle displayRectangle = Display.getCurrent().getClientArea();

		int xLocation = (displayRectangle.width - splashRectangle.width) / 2;

		int yLocation = (displayRectangle.height - splashRectangle.height) / 2;

		ManiSplash.splashShell.setLocation(xLocation, yLocation);

		ManiSplash.splashShell.open();

		this.display.asyncExec(this.startupTask);

		while (!ManiSplash.applicationShell.isDisposed()) {
			if (!this.display.readAndDispatch ()) this.display.sleep ();
		}
		
	}

	public void setStartupTask(Runnable task) {
		this.startupTask = task;
	}
	
}