package br.com.mani.app.swt.component;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.mani.app.mvc.controller.LoginController;
import br.com.mani.app.swt.composite.EntryComposite;
import br.com.mani.app.swt.composite.LoginComposite;
import br.com.mani.app.swt.util.ResourceConstants;
import br.com.mani.app.swt.util.ResourceUtil;

@Component
public class UIManager {
	
	public static final Shell applicationShell = new Shell(Display.getDefault());
	
	public static final Shell splashShell = new Shell(SWT.ON_TOP);
	
	private Display display = Display.getDefault();
	
	@Autowired
	private LoginController loginController;
	
	private Composite currentView;
	
	public void startApp() {
		
		splashShell.dispose();
		
		openLoginUI();
		
	}
	
	public void openLoginUI() {
	
		LoginComposite root = new LoginComposite(getShell(), SWT.BORDER, this.loginController);
		
		FormData formData = new FormData();
		
		formData.top = new FormAttachment(0);
		
		formData.left = new FormAttachment(0);
		
		formData.right = new FormAttachment(100);
		
		formData.bottom = new FormAttachment(100);
		
		root.setLayoutData(formData);

		root.pack();

		getShell().setImage(ResourceUtil.getImage("resume-letter.png",ResourceConstants.ImageIco));

		getShell().pack();

		Monitor primary = this.display.getPrimaryMonitor();
		Rectangle bounds = primary.getClientArea();

		//if(this.centralize){
			Rectangle rect = UIManager.applicationShell.getClientArea();
			int x = bounds.x + (bounds.width - rect.width) / 2;
			int y = bounds.y + (bounds.height - rect.height) / 2;
			UIManager.applicationShell.setLocation(x,y);
//		}else{
//			this.shell.setBounds(bounds);
//			this.shell.setLocation(0,0);
//			this.shell.setMaximized(true);
//		}

		UIManager.applicationShell.setAlpha(255);
		UIManager.applicationShell.setActive();
		UIManager.applicationShell.open();
		
		setCurrentView(root);
		
	}

	
	public void openLancamentoUI() {
		
		EntryComposite root = new EntryComposite(getShell(), SWT.BORDER);
		
		FormData formData = new FormData();
		
		formData.top = new FormAttachment(0);
		
		formData.left = new FormAttachment(0);
		
		formData.right = new FormAttachment(100);
		
		formData.bottom = new FormAttachment(100);
		
		root.setLayoutData(formData);

		root.pack();

		getShell().setImage(ResourceUtil.getImage("resume-letter.png",ResourceConstants.ImageIco));

		getShell().pack();
		
		Monitor primary = this.display.getPrimaryMonitor();
		Rectangle bounds = primary.getClientArea();

		//if(this.centralize){
			Rectangle rect = UIManager.applicationShell.getClientArea();
			int x = bounds.x + (bounds.width - rect.width) / 2;
			int y = bounds.y + (bounds.height - rect.height) / 2;
			UIManager.applicationShell.setLocation(x,y);
//		}else{
//			this.shell.setBounds(bounds);
//			this.shell.setLocation(0,0);
//			this.shell.setMaximized(true);
//		}

		UIManager.applicationShell.setAlpha(255);
		UIManager.applicationShell.setActive();
		UIManager.applicationShell.open();
		
		setCurrentView(root);
		
		
	}
	
	public Shell getShell() {
		return UIManager.applicationShell;
	}
	
	private void setCurrentView(Composite composite) {
		if(this.currentView != null) {
			this.currentView.dispose();
		}
		this.currentView = composite;
	}
	
	public void showError(String mensagem) {
		
		 MessageBox messageBox = new MessageBox(UIManager.applicationShell, SWT.ERROR);
		 
		 messageBox.setMessage(mensagem);
		 
		 messageBox.open();
		
	}
	
	
}
