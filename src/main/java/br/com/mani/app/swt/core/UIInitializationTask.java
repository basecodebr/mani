package br.com.mani.app.swt.core;

import org.eclipse.swt.widgets.Shell;

public interface UIInitializationTask <T>{

    Shell execute(T session);
    
    default String getDescription(){
    	return "Initializing UI...";
    }
    
}
