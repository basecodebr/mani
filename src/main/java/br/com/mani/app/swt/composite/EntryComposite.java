package br.com.mani.app.swt.composite;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

public class EntryComposite extends Composite{

	private DateTime dtPayment;
	private DateTime dtDebt;
	private Combo cbCategs;
	private Text txDesc;
	private Text txVal;
	private Combo cbAccount;
	private Combo cbPayForm;
	private Combo cbCreditCard;
	private Spinner spnNumParcels;
	private Button ckPayd;
	private Button btConfCard;
	private Button btConfCategory;
	private Button btConfAccount;
	private Button ckRepeatValue;
	private Button ckDivideValue;
	private Label lbCard;
	private Label lbTransfAccount;
	private Combo cbTransfAccount;
	private Combo cbParcels;
	private Button btConfTransfAccount;
	//private Button ckTithing;
	private Text txaObs;
	private Label lbParcels;
	private Label lbRepeatValue;
	private Label lbDivideValue;
	private Label lbParcelInfo;

	public static final String ID = "lancamentoComposite";
	public static final String ID_DIALOG = "lancamentoDialog";

	public EntryComposite(Composite parent, int style) {
		super(parent, style);
		createContents();
		//updateView();
	}

	protected void checkParc(){
		//boolean editing = this.model.isEditing();
		//ckRepeatValue.setEnabled(!editing);
		//ckDivideValue.setEnabled(!editing);		
	}

//	@Override
//	public void updateView(){
//
//		//model state.
//		DateModel dateModel = null;
//		boolean adding = this.model.isAdding();
//
//		Integer  parcelType = this.model.getTipoParcelamento();
//		//boolean divided = parcelType == EntryModel.PARCEL;
//
//		boolean editingParcel = parcelType == EntryModel.PARCEL && model.isEditing();
//		boolean card = model.isCartaoSelecionado();
//		boolean transf = model.isTransferenciaSelecionada();
//
//		//update combo values
//		cbTransfAccount.setItems(model.getContas());
//		cbAccount.setItems(model.getContas());
//		cbCategs.setItems(model.getCategories());
//		cbCreditCard.setItems(model.getCartoes());
//
//		//select combos
//		cbCategs.select(model.getCategory());
//		cbAccount.select(model.getContaCorrente());
//		cbTransfAccount.select(model.getContaTransferencia());
//		cbPayForm.select(model.getFormaPgto());
//		cbCreditCard.select(model.getCreditCard());
//		ckPayd.setSelection(model.isPago());
//		//ckTithing.setSelection(model.isConsDizimo());
//
//		//select checks
//		this.ckDivideValue.setSelection(adding && parcelType == EntryModel.PARCEL);
//		this.ckRepeatValue.setSelection(adding && parcelType == EntryModel.REPLICATION);
//
//		//pack combos
//		cbCategs.pack();
//		cbTransfAccount.pack();
//		cbCreditCard.pack();
//
//		//update dates
//		dateModel = model.getDataPgto();
//		dtPayment.setDate(dateModel.year, dateModel.month, dateModel.day);
//
//		dateModel = model.getDebtDate();
//		dtDebt.setDate(dateModel.year, dateModel.month, dateModel.day);
//
//		//update texts and others
//		spnNumParcels.setSelection(model.getNumeroParcelas());
//		txaObs.setText(model.getObservacao());
//		txDesc.setText(model.getDescription());
//		txVal.setText(model.getValue());
//
//		String parcelInfo = this.model.getParcelInfo();
//		if(parcelInfo != null){
//			this.lbParcelInfo.setText(this.model.getParcelInfo());
//		}
//
//		spnNumParcels.setSelection(0);
//		ckDivideValue.setSelection(parcelType == EntryModel.PARCEL && !editingParcel);
//		ckRepeatValue.setSelection(parcelType == EntryModel.REPLICATION);
//
//		//enable/disable.
//
//		dtPayment.setEnabled(!editingParcel);
//		dtDebt.setEnabled(!editingParcel);
//		cbCategs.setEnabled(!editingParcel);
//		txDesc.setEnabled(!editingParcel);
//		txaObs.setEnabled(!editingParcel);
//		txVal.setEnabled(!editingParcel);
//		cbAccount.setEnabled(!editingParcel);
//		cbPayForm.setEnabled(!editingParcel);
//		cbTransfAccount.setEnabled(transf && !editingParcel);
//		cbCreditCard.setEnabled(card && cbPayForm.isEnabled() && !editingParcel);
//
//		//ckTithing.setEnabled(!editingParcel);
//
//		btConfTransfAccount.setEnabled(cbTransfAccount.isEnabled() && !editingParcel);
//		btConfCard.setEnabled(cbCreditCard.isEnabled() && !editingParcel);
//		btConfAccount.setEnabled(!editingParcel);
//		btConfCategory.setEnabled(!editingParcel);
//
//		ckDivideValue.setEnabled(adding && !editingParcel);
//		ckRepeatValue.setEnabled(adding && !editingParcel);
//		ckPayd.setEnabled(!editingParcel);
//		
//
//		//spnNumParcels.setEnabled(adding && (ckDivideValue.getSelection() || ckRepeatValue.getSelection()));
//		cbParcels.setEnabled(adding && (ckDivideValue.getSelection() || ckRepeatValue.getSelection()));
//
//		Button saveButton = this.session.actionMapper.getButton("svLa_button");
//		saveButton.setEnabled(!editingParcel);
//
//		Label messageLabel = this.session.actionMapper.getLabel("entryMessages_label");
//		refreshMessage(messageLabel, this.model.messageModel);
//		messageLabel.pack();
//		this.model.messageModel.clearMessage();
//		
//		this.layout(true);
//	}

//	public void updateModel(){
//		model.setDataPgto(dtPayment.getDay(), dtPayment.getMonth(), dtPayment.getYear());
//		model.setDebtDate(dtDebt.getDay(), dtDebt.getMonth(), dtDebt.getYear());
//		model.setCategory(cbCategs.getSelectionIndex());
//		model.setDescription(txDesc.getText());
//		model.setValue(txVal.getText());
//		model.setContaCorrente(cbAccount.getSelectionIndex());
//		model.setContaTransferencia(cbTransfAccount.getSelectionIndex());
//		model.setPago(ckPayd.getSelection());
//		//model.setConsDizimo(ckTithing.getSelection());
//		model.setFormaPgto(cbPayForm.getSelectionIndex());
//
//		if(ckDivideValue.getSelection()){
//			model.setParcelType(EntryModel.PARCEL);
//		}else if(ckRepeatValue.getSelection()){
//			model.setParcelType(EntryModel.REPLICATION);
//		}else{
//			model.setParcelType(EntryModel.NONE);
//		}
//
//		model.setObservacao(txaObs.getText());
//		
//		if(spnNumParcels.isEnabled()){
//			model.setNumeroParcelas(spnNumParcels.getSelection());
//		}else if(cbParcels.isEnabled()){
//			int index = cbParcels.getSelectionIndex();
//			if(index > 0){
//				model.setNumeroParcelas(index + 1);
//			}
//		}
//
//	}

//	private void checkFormaPgto(){
//		if(model.isCartaoSelecionado()){
//			cbTransfAccount.deselectAll();
//		}else if(model.isTransferenciaSelecionada()){
//			cbCreditCard.deselectAll();
//		}
//	}

	protected void createContents(){

		//ActionBuilder builder = getSession().getActionBuilder();
		//MessageProvider messages = session.messages;

		GridLayout layout = new GridLayout(4,false);
		setLayout(layout);

		GridData gridData = null;

		//CompositeUtil.getInstance().criarCabecalho(messages.label(Statizo.STR_ENTRY), this, 4);

		//parcel info/warn START

		this.lbParcelInfo = new Label(this, SWT.NONE);
		this.lbParcelInfo.setForeground(Display.getDefault().getSystemColor(SWT.COLOR_BLUE));
		this.lbParcelInfo.setLayoutData(new RowData());
		this.lbParcelInfo.setAlignment(SWT.CENTER);

		gridData = new GridData(SWT.FILL, SWT.FILL, true,true,4,1);
		this.lbParcelInfo.setLayoutData(gridData);

		//parcel info/warn END

		Label label = new Label(this,SWT.NONE);
		label.setText("Data de pagamento");

		gridData = new GridData();
		gridData.horizontalAlignment = GridData.END;
		label.setLayoutData(gridData);

		this.dtPayment = new DateTime(this,SWT.NONE);
		gridData = new GridData();
		gridData.horizontalSpan = 3;
		this.dtPayment.setLayoutData(gridData);

//		if(this.model.changeDebtDateOnChangePayDate){
//			dtPayment.addSelectionListener(new SelectionAdapter() {
//				public void widgetSelected(SelectionEvent e) {
//					dtDebt.setDate(dtPayment.getYear(), dtPayment.getMonth(), dtPayment.getDay());
//				};
//			});
//		}

		label = new Label(this,SWT.NONE);
		label.setText("Data da dívida");

		gridData = new GridData();
		gridData.horizontalAlignment = GridData.END;
		label.setLayoutData(gridData);

		this.dtDebt = new DateTime(this, SWT.NONE);
		gridData = new GridData();
		gridData.horizontalSpan = 3;
		this.dtDebt.setLayoutData(gridData);

		label = new Label(this,SWT.NONE);
		label.setText("Categoria");

		gridData = new GridData();
		gridData.horizontalAlignment = GridData.END;		
		label.setLayoutData(gridData);

		this.cbCategs = new Combo(this, SWT.NONE | SWT.READ_ONLY);
		gridData = new GridData();
		this.cbCategs.setLayoutData(gridData);
		//this.cbCategs.setItems(model.getCategories());

		this.btConfCategory = new Button(this,SWT.PUSH);

		gridData = new GridData();
		gridData.horizontalAlignment = GridData.BEGINNING;
		gridData.horizontalSpan = 2;
		this.btConfCategory.setLayoutData(gridData);
		//builder.build(this.btConfCategory, model.getConfigCategoryAction());

		label = new Label(this,SWT.NONE);
		label.setText("Descrição");

		gridData = new GridData();
		gridData.horizontalAlignment = GridData.END;
		label.setLayoutData(gridData);

		this.txDesc = new Text(this, SWT.BORDER);
		gridData = new GridData();
		gridData.horizontalSpan = 3;
		gridData.widthHint = 325;
		this.txDesc.setLayoutData(gridData);

		label = new Label(this,SWT.NONE);
		label.setText("Observações");

		gridData = new GridData();
		gridData.horizontalAlignment = GridData.END;
		label.setLayoutData(gridData);

		this.txaObs = new Text(this, SWT.MULTI | SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
		gridData = new GridData(GridData.FILL_HORIZONTAL);
		gridData.horizontalSpan = 3;
		gridData.heightHint = 40;
		this.txaObs.setLayoutData(gridData);
		//ActionBuilder.setTabBehavior(this.txaObs);

		label = new Label(this,SWT.NONE);
		label.setText("Valor");

		gridData = new GridData();
		gridData.horizontalAlignment = GridData.END;		
		label.setLayoutData(gridData);

		this.txVal = new Text(this, SWT.BORDER);
		gridData = new GridData();
		gridData.horizontalSpan = 3;
		gridData.widthHint = 200;
		this.txVal.setLayoutData(gridData);

		label = new Label(this,SWT.NONE);
		label.setText("Conta Corrente");

		gridData = new GridData();
		gridData.horizontalAlignment = GridData.END;
		label.setLayoutData(gridData);

		this.cbAccount = new Combo(this, SWT.NONE | SWT.READ_ONLY);
		gridData = new GridData();
		this.cbAccount.setLayoutData(gridData);
		//this.cbAccount.setItems(model.getContas());
		this.cbAccount.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
//				updateModel();
//				model.getCreditCardConverter().applyFilter();
//				updateView();
			}
		});

		this.btConfAccount = new Button(this,SWT.PUSH);
		//builder.build(this.btConfAccount, model.getConfigContaAction());

		gridData = new GridData();
		gridData.horizontalAlignment = GridData.BEGINNING;
		gridData.horizontalSpan = 2;
		this.btConfAccount.setLayoutData(gridData);

		label = new Label(this,SWT.NONE);
		label.setText("Forma de pagamento");

		gridData = new GridData();
		gridData.horizontalAlignment = GridData.END;		
		label.setLayoutData(gridData);

		this.cbPayForm = new Combo(this, SWT.NONE | SWT.READ_ONLY);
		gridData = new GridData();
		gridData.horizontalSpan = 3;
		this.cbPayForm.setLayoutData(gridData);
		//this.cbPayForm.setItems(model.getFormasPagamento());

		this.cbPayForm.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
//				model.setFormaPgto(EntryComposite.this.cbPayForm.getSelectionIndex());
//				checkFormaPgto();
//				updateModel();
//				updateView();
			}
		} );

		this.lbCard = new Label(this,SWT.NONE);
		this.lbCard.setText("Cartão de Crédito");

		gridData = new GridData();
		gridData.horizontalAlignment = GridData.END;		
		this.lbCard.setLayoutData(gridData);

		this.cbCreditCard = new Combo(this, SWT.NONE | SWT.READ_ONLY);
		//this.cbCreditCard.setItems(model.getCartoes());

		this.cbCreditCard.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
//				updateModel();
//				model.setCreditCard(EntryComposite.this.cbCreditCard.getSelectionIndex());
//				model.updatePaymentDate();
//				updateView();
			}
		});

		gridData = new GridData();
		this.cbCreditCard.setLayoutData(gridData);

		this.btConfCard = new Button(this,SWT.PUSH);
		//builder.build(this.btConfCard, model.getConfigCartaoAction());
		gridData = new GridData();
		gridData.horizontalAlignment = GridData.BEGINNING;
		gridData.horizontalSpan = 2;
		this.btConfCard.setLayoutData(gridData);

		this.lbTransfAccount = new Label(this,SWT.NONE);
		this.lbTransfAccount.setText("Transferir para");

		gridData = new GridData();
		gridData.horizontalAlignment = GridData.END;		
		this.lbTransfAccount.setLayoutData(gridData);

		this.cbTransfAccount = new Combo(this, SWT.NONE | SWT.READ_ONLY);
		gridData = new GridData();
		this.cbTransfAccount.setLayoutData(gridData);
		//this.cbTransfAccount.setItems(model.getContas());

		this.btConfTransfAccount = new Button(this,SWT.PUSH);
		//builder.build(this.btConfTransfAccount, model.getConfigContaTransfAction());

		gridData = new GridData();
		gridData.horizontalSpan = 2;
		this.btConfTransfAccount.setLayoutData(gridData);

		this.lbDivideValue = new Label(this,SWT.NONE);
		this.lbDivideValue.setText("Parcelar em");
		this.lbDivideValue.setAlignment(SWT.RIGHT);
		
		gridData = new GridData();
		gridData.horizontalAlignment = GridData.END;
		this.lbDivideValue.setLayoutData(gridData);

		this.ckDivideValue = new Button(this,SWT.CHECK);
		this.ckDivideValue.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(EntryComposite.this.ckDivideValue.getSelection()){
					EntryComposite.this.ckRepeatValue.setSelection(false);
				}
				//updateModel();
				//updateView();
			}
		});

		gridData = new GridData(SWT.BEGINNING);
		this.ckDivideValue.setLayoutData(gridData);		
		
		
		this.lbRepeatValue = new Label(this,SWT.NONE);
		this.lbRepeatValue.setText("Repetir");
		gridData = new GridData();
		gridData.horizontalAlignment = GridData.END;
		this.lbRepeatValue.setLayoutData(gridData);

		this.ckRepeatValue = new Button(this,SWT.CHECK);
		this.ckRepeatValue.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(EntryComposite.this.ckRepeatValue.getSelection()){
					EntryComposite.this.ckDivideValue.setSelection(false);
				}
				//updateModel();
				//updateView();
			}
		});

		gridData = new GridData(SWT.BEGINNING);
		this.ckRepeatValue.setLayoutData(gridData);

		
		this.lbParcels = new Label(this,SWT.NONE);
		this.lbParcels.setText("Quantas vezes?");
		gridData = new GridData();
		gridData.horizontalAlignment = GridData.END;
		gridData.grabExcessHorizontalSpace = true;
		this.lbParcels.setLayoutData(gridData);

		this.cbParcels = new Combo(this, SWT.NONE | SWT.READ_ONLY);
		this.cbParcels.setEnabled(false);
		gridData = new GridData();
		this.cbParcels.setLayoutData(gridData);
		//this.cbParcels.setItems(model.getParcels());
		this.cbParcels.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(EntryComposite.this.cbParcels.getSelectionIndex() == 12){
					EntryComposite.this.spnNumParcels.setEnabled(true);
				}else{
					EntryComposite.this.spnNumParcels.setSelection(0);
					EntryComposite.this.spnNumParcels.setEnabled(false);
				}

			}
		});
		
		
		this.spnNumParcels = new Spinner(this, SWT.NONE);
		this.spnNumParcels.setEnabled(false);
		gridData = new GridData();
		gridData.minimumWidth = 60;
		gridData.horizontalSpan = 2;
		gridData.grabExcessHorizontalSpace = true;
		gridData.horizontalAlignment = GridData.BEGINNING;
		this.spnNumParcels.setLayoutData(gridData);

		label = new Label(this,SWT.NONE);
		gridData = new GridData();
		gridData.horizontalAlignment = GridData.END;
		label.setLayoutData(gridData);
		label.setText("Pago");

		this.ckPayd = new Button(this,SWT.CHECK);
		gridData = new GridData();
		gridData.horizontalSpan = 3;
		this.ckPayd.setLayoutData(gridData);

		//builder.build(this, this.model.actions,4);

	}
	
	

}