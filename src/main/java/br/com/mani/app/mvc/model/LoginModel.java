package br.com.mani.app.mvc.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor(access=AccessLevel.PRIVATE)
public class LoginModel {
	
	private String usuario;
	
	private String senha;
	
}
