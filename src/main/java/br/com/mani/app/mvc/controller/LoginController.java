package br.com.mani.app.mvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.mani.app.component.AppContext;
import br.com.mani.app.mvc.model.LoginModel;
import br.com.mani.app.service.AuthService;
import br.com.mani.app.swt.component.UIManager;

@Component
public class LoginController {

	@Autowired
	AppContext appContext;

	@Autowired
	UIManager uiManager;
	
	@Autowired
	AuthService authService;
	
	public void autenticar(LoginModel model) {
		
		try {
			
			final Long usuarioId = this.authService.autenticar(model);
			
			this.appContext.setUsuarioId(usuarioId);
			
			this.uiManager.openLancamentoUI();

		}catch(Exception e) {

			this.uiManager.showError(e.getMessage());
			
		}
		
	}
	
}
